package com.fif.tech.android.android_digitalcard;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

public class Dijkstra {

    public static final String SAN_JOAQUIN = "San Joaquin";

    public static final String CAMINO_AGRICOLA = "Camino Agricola";

    public static final String CARLOS_VALDOVINOS = "Carlos Valdovinos";

    public static final String RODRIGO_ARAYA = "Rodrigo Araya";

    public static final String NUBLE_5 = "Ñuble 5";

    public static final String NUBLE_6 = "Ñuble 6";

    public static final String IRARRAZAVAL_5 = "Irarrazaval 5";

    public static final String IRARRAZAVAL_3 = "Irarrazaval 3";

    public static final String SANTA_ISABLE = "Santa Isabel";

    public static final String PARQUE_BUSTAMANTE = "Parque Bustamante";

    public static final String BAQUEDANO_5 = "Baquedano 5";

    public static final String BAQUEDANO_1 = "Baquedano 1";

    public static final String BELLA_ARTES = "Bellas Artes";

    public static final String PLAZA_ARMAS_5 = "Plaza de Armas 5";

    public static final String PLAZA_ARMAS_3 = "Plaza de Armas 3";

    public static final String SANTA_ANA_5 = "Santa Ana 5";

    public static final String SANTA_ANA_2 = "Santa Ana 2";

    public static final String CUMMING = "Cumming";

    public static final String PRESIDENTE_PEDRO_AGUIRRE_CERDA = "Presidente Pedro Aguirre Cerda";

    public static final String FRANKLIN_6 = "Frannklin 6";

    public static final String FRANKLIN_2 = "Frannklin 2";

    public static final String BIO_BIO = "Bio Bio";

    public static final String ESTADIO_NACIONAL = "Estadio Nacional";

    public static final String NUNOA_6 = "Ñuñoa 6";

    public static final String NUNOA_3 = "Ñuñoa 3";

    public static final String INEZ_SUAREZ = "Inez de Suarez";

    public static final String LOS_LEONES_6 = "Los Leones 6";

    public static final String LOS_LEONES_1 = "Los Leones 1";

    public static final String U_L_A = "Union Latino Americana";

    public static final String REPUBLICA = "Republica";

    public static final String LOS_HEROES_1 = "Los Heroes 1";

    public static final String LOS_HEROES_2 = "Los Heroes 2";

    public static final String MONEDA = "Moneda";

    public static final String U_CHILE_1 = "Universidad de Chile 1";

    public static final String U_CHILE_3 = "Universidad de Chile 3";

    public static final String SANTA_LUCIA = "Santa Lucia";

    public static final String U_CATOLICA= "Universidad Catolica";

    public static final String SALVADOR= "Salvador";

    public static final String MANUEL_MONTT= "Manuel Montt";

    public static final String PEDRO_VALDIVIA= "Pedro Valdivia";

    public static final String TOESCA= "Toesca";

    public static final String PARQUE_OHIGGINS= "Parque Ohigginns";

    public static final String RONDIZZONI= "Rondizzoni";

    public static final String EL_LLANO= "El Llano";

    public static final String SAN_MIGUEL= "San Miguel";

    public static final String PARQUE_ALMAGRO= "Parque Almagro";

    public static final String MATA= "Mata";

    public static final String MONSENOR_AGUIRRE= "Monseñor de Aguirre";

    public static final String CHILE_ESPAÑA= "Chile España";


    public static void main(String[] args) {
        Graph g = new Graph();

        //todo: linea 5
        g.addVertex(SAN_JOAQUIN, Arrays.asList(new Vertex(CAMINO_AGRICOLA, 2)));
        g.addVertex(CAMINO_AGRICOLA, Arrays.asList(new Vertex(SAN_JOAQUIN, 2), new Vertex(CARLOS_VALDOVINOS, 2)));
        g.addVertex(CARLOS_VALDOVINOS, Arrays.asList(new Vertex(CAMINO_AGRICOLA, 2), new Vertex(RODRIGO_ARAYA, 2)));
        g.addVertex(RODRIGO_ARAYA, Arrays.asList(new Vertex(CARLOS_VALDOVINOS, 2), new Vertex(NUBLE_5, 2)));
        g.addVertex(NUBLE_5, Arrays.asList(new Vertex(RODRIGO_ARAYA, 2), new Vertex(NUBLE_6, 3),new Vertex(IRARRAZAVAL_5, 2)));
        g.addVertex(IRARRAZAVAL_5, Arrays.asList(new Vertex(NUBLE_5, 2), new Vertex(IRARRAZAVAL_3, 3),new Vertex(SANTA_ISABLE, 2)));
        g.addVertex(SANTA_ISABLE, Arrays.asList(new Vertex(IRARRAZAVAL_5, 2), new Vertex(PARQUE_BUSTAMANTE, 2)));
        g.addVertex(PARQUE_BUSTAMANTE, Arrays.asList(new Vertex(SANTA_ISABLE, 2), new Vertex(BAQUEDANO_5, 2)));
        g.addVertex(BAQUEDANO_5, Arrays.asList(new Vertex(PARQUE_BUSTAMANTE, 2), new Vertex(BAQUEDANO_1, 3), new Vertex(BELLA_ARTES, 2)));
        g.addVertex(BELLA_ARTES, Arrays.asList(new Vertex(BAQUEDANO_5, 2), new Vertex(PLAZA_ARMAS_5, 2)));
        g.addVertex(PLAZA_ARMAS_5, Arrays.asList(new Vertex(BELLA_ARTES, 2), new Vertex(PLAZA_ARMAS_3, 3), new Vertex(SANTA_ANA_5, 2)));
        g.addVertex(SANTA_ANA_5, Arrays.asList(new Vertex(PLAZA_ARMAS_5, 2), new Vertex(SANTA_ANA_2, 3), new Vertex(CUMMING, 2)));
        g.addVertex(CUMMING, Arrays.asList(new Vertex(SANTA_ANA_5, 2)));

        //todo: linea 6
        g.addVertex(PRESIDENTE_PEDRO_AGUIRRE_CERDA, Arrays.asList(new Vertex(FRANKLIN_6, 2)));
        g.addVertex(FRANKLIN_6, Arrays.asList(new Vertex(PRESIDENTE_PEDRO_AGUIRRE_CERDA, 2),new Vertex(FRANKLIN_2, 3), new Vertex(BIO_BIO, 2)));
        g.addVertex(BIO_BIO, Arrays.asList(new Vertex(FRANKLIN_6, 2), new Vertex(NUBLE_6, 2)));
        g.addVertex(NUBLE_6, Arrays.asList(new Vertex(BIO_BIO, 2),new Vertex(NUBLE_5, 3), new Vertex(ESTADIO_NACIONAL, 2)));
        g.addVertex(ESTADIO_NACIONAL, Arrays.asList(new Vertex(NUBLE_6, 2), new Vertex(NUNOA_6, 2)));
        g.addVertex(NUNOA_6, Arrays.asList(new Vertex(ESTADIO_NACIONAL, 2),new Vertex(NUNOA_3, 3), new Vertex(INEZ_SUAREZ, 2)));
        g.addVertex(INEZ_SUAREZ, Arrays.asList(new Vertex(NUNOA_6, 2), new Vertex(LOS_LEONES_6, 2)));
        g.addVertex(LOS_LEONES_6, Arrays.asList(new Vertex(INEZ_SUAREZ, 2), new Vertex(LOS_LEONES_1, 3)));

        //todo: linea 1
        g.addVertex(U_L_A, Arrays.asList(new Vertex(REPUBLICA, 2)));
        g.addVertex(REPUBLICA, Arrays.asList(new Vertex(U_L_A, 2), new Vertex(LOS_HEROES_1, 2)));
        g.addVertex(LOS_HEROES_1, Arrays.asList(new Vertex(REPUBLICA, 2), new Vertex(LOS_HEROES_2, 3), new Vertex(MONEDA, 2)));
        g.addVertex(MONEDA, Arrays.asList(new Vertex(LOS_HEROES_1, 2), new Vertex(U_CHILE_1, 2)));
        g.addVertex(U_CHILE_1, Arrays.asList(new Vertex(MONEDA, 2), new Vertex(U_CHILE_3, 3), new Vertex(SANTA_LUCIA, 2)));
        g.addVertex(SANTA_LUCIA, Arrays.asList(new Vertex(U_CHILE_1, 2), new Vertex(U_CATOLICA, 2)));
        g.addVertex(U_CATOLICA, Arrays.asList(new Vertex(SANTA_LUCIA, 2), new Vertex(BAQUEDANO_1, 2)));
        g.addVertex(BAQUEDANO_1, Arrays.asList(new Vertex(U_CATOLICA, 2), new Vertex(BAQUEDANO_5, 3), new Vertex(SALVADOR, 2)));
        g.addVertex(SALVADOR, Arrays.asList(new Vertex(BAQUEDANO_1, 2), new Vertex(MANUEL_MONTT, 2)));
        g.addVertex(MANUEL_MONTT, Arrays.asList(new Vertex(SALVADOR, 2), new Vertex(PEDRO_VALDIVIA, 2)));
        g.addVertex(PEDRO_VALDIVIA, Arrays.asList(new Vertex(MANUEL_MONTT, 2), new Vertex(LOS_LEONES_1, 2)));
        g.addVertex(LOS_LEONES_1, Arrays.asList(new Vertex(PEDRO_VALDIVIA, 2), new Vertex(LOS_LEONES_6, 3 )));

        //todo: line 2
        g.addVertex(SAN_MIGUEL, Arrays.asList(new Vertex(EL_LLANO, 2)));
        g.addVertex(EL_LLANO, Arrays.asList(new Vertex(SAN_MIGUEL, 2), new Vertex(FRANKLIN_2, 2)));
        g.addVertex(FRANKLIN_2, Arrays.asList(new Vertex(EL_LLANO, 2), new Vertex(FRANKLIN_6, 3),new Vertex(RONDIZZONI, 2)));
        g.addVertex(RONDIZZONI, Arrays.asList(new Vertex(FRANKLIN_2, 2), new Vertex(PARQUE_OHIGGINS, 2)));
        g.addVertex(PARQUE_OHIGGINS, Arrays.asList(new Vertex(RONDIZZONI, 2), new Vertex(TOESCA, 2)));
        g.addVertex(TOESCA, Arrays.asList(new Vertex(PARQUE_OHIGGINS, 2), new Vertex(LOS_HEROES_2, 2)));
        g.addVertex(LOS_HEROES_2, Arrays.asList(new Vertex(TOESCA, 2), new Vertex(LOS_HEROES_1, 3),new Vertex(SANTA_ANA_2, 2)));
        g.addVertex(SANTA_ANA_2, Arrays.asList(new Vertex(LOS_HEROES_2, 2), new Vertex(SANTA_ANA_5, 3)));

        //todo: linea 3
        g.addVertex(PLAZA_ARMAS_3, Arrays.asList(new Vertex(PLAZA_ARMAS_5, 3),new Vertex(U_CHILE_3, 2) ));
        g.addVertex(U_CHILE_3, Arrays.asList(new Vertex(PLAZA_ARMAS_3, 3),new Vertex(U_CHILE_1, 3), new Vertex(PARQUE_ALMAGRO, 2)));
        g.addVertex(PARQUE_ALMAGRO, Arrays.asList(new Vertex(U_CHILE_3, 2),new Vertex(MATA, 2) ));
        g.addVertex(MATA, Arrays.asList(new Vertex(PARQUE_ALMAGRO, 2),new Vertex(IRARRAZAVAL_3, 2)));
        g.addVertex(IRARRAZAVAL_3, Arrays.asList(new Vertex(MATA, 2),new Vertex(IRARRAZAVAL_5, 3),new Vertex(MONSENOR_AGUIRRE, 2)));
        g.addVertex(MONSENOR_AGUIRRE, Arrays.asList(new Vertex(IRARRAZAVAL_3, 2),new Vertex(NUNOA_3, 2)));
        g.addVertex(NUNOA_3, Arrays.asList(new Vertex(MONSENOR_AGUIRRE, 2),new Vertex(NUNOA_6, 3), new Vertex(CHILE_ESPAÑA, 2)));
        g.addVertex(CHILE_ESPAÑA, Arrays.asList(new Vertex(NUNOA_3, 2)));



        /*g.addVertex('A', Arrays.asList(new Vertex('B', 7), new Vertex('C', 8)));
        g.addVertex('B', Arrays.asList(new Vertex('A', 7), new Vertex('F', 2)));
        g.addVertex('C', Arrays.asList(new Vertex('A', 8), new Vertex('F', 6), new Vertex('G', 4)));
        g.addVertex('D', Arrays.asList(new Vertex('F', 8)));
        g.addVertex('E', Arrays.asList(new Vertex('H', 1)));
        g.addVertex('F', Arrays.asList(new Vertex('B', 2), new Vertex('C', 6), new Vertex('D', 8), new Vertex('G', 9), new Vertex('H', 3)));
        g.addVertex('G', Arrays.asList(new Vertex('C', 4), new Vertex('F', 9)));
        g.addVertex('H', Arrays.asList(new Vertex('E', 1), new Vertex('F', 3)));*/
        System.out.println(g.getShortestPath(U_CHILE_3, ESTADIO_NACIONAL));
    }
}

class Vertex implements Comparable<Vertex> {

    private String id;
    private Integer distance;

    public Vertex(String id, Integer distance) {
        super();
        this.id = id;
        this.distance = distance;
    }

    public String getId() {
        return id;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((distance == null) ? 0 : distance.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Vertex other = (Vertex) obj;
        if (distance == null) {
            if (other.distance != null)
                return false;
        } else if (!distance.equals(other.distance))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Vertex [id=" + id + ", distance=" + distance + "]";
    }

    @Override
    public int compareTo(Vertex o) {
        if (this.distance < o.distance)
            return -1;
        else if (this.distance > o.distance)
            return 1;
        else
            return this.getId().compareTo(o.getId());
    }

}

class Graph {

    private final Map<String, List<Vertex>> vertices;

    public Graph() {
        this.vertices = new HashMap<String, List<Vertex>>();
    }

    public void addVertex(String name, List<Vertex> vertex) {
        this.vertices.put(name, vertex);
    }

    public Map<String,Integer> getShortestPath(String start, String finish) {
        final Map<String, Integer> distances = new LinkedHashMap<>();
        final Map<String, Vertex> previous = new HashMap<String, Vertex>();
        PriorityQueue<Vertex> nodes = new PriorityQueue<Vertex>();

        for(String vertex : vertices.keySet()) {
            if (vertex == start) {
                distances.put(vertex, 0);
                nodes.add(new Vertex(vertex, 0));
            } else {
                distances.put(vertex, Integer.MAX_VALUE);
                nodes.add(new Vertex(vertex, Integer.MAX_VALUE));
            }
            previous.put(vertex, null);
        }

        while (!nodes.isEmpty()) {
            Vertex smallest = nodes.poll();
            if (smallest.getId() == finish) {
                final Map<String,Integer> path = new LinkedHashMap<>();
                while (previous.get(smallest.getId()) != null) {
                    path.put(smallest.getId(),smallest.getDistance());
                    smallest = previous.get(smallest.getId());
                }
                return path;
            }

            if (distances.get(smallest.getId()) == Integer.MAX_VALUE) {
                break;
            }

            for (Vertex neighbor : vertices.get(smallest.getId())) {
                Integer alt = distances.get(smallest.getId()) + neighbor.getDistance();
                if (alt < distances.get(neighbor.getId())) {
                    distances.put(neighbor.getId(), alt);
                    previous.put(neighbor.getId(), smallest);

                    forloop:
                    for(Vertex n : nodes) {
                        if (n.getId() == neighbor.getId()) {
                            nodes.remove(n);
                            n.setDistance(alt);
                            nodes.add(n);
                            break forloop;
                        }
                    }
                }
            }
        }

        return distances;
        //return new ArrayList<String>(distances.keySet());
    }
}